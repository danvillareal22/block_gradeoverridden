<?php

require_once('../../config.php');

$q = required_param('q', PARAM_TEXT);

$q = $q . '%';

$matches = $DB->get_records_sql("SELECT U.* FROM {user} U WHERE U.mnethostid = 1 AND U.username LIKE ? LIMIT 100", array($q));

$output = new stdClass();
$output->totalcount = count($matches);
$output->incomplete_results = true;
$output->items = array();

foreach ($matches as $match) {
	$item = new stdClass();
	$item->id = (int) $match->id;
	$item->text = '[' . $match->username . '] ' . $match->firstname . ' ' . $match->lastname;
	$output->items[] = $item;
}

echo json_encode($output);