$(document).ready(function() {

	$('input#block_gradeoverride_dategraded').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        format : 'DD/MM/YYYY',
        maxDate : moment().format('DD/MM/YYYY'),
    });

    $("#block_gradeoverride_gradeitems").select2({});

    $("#block_gradeoverride_userids").select2({
    	ajax: {
	    url: "/blocks/gradeoverride/ajaxuserlist.php",
	    dataType: 'json',
	    delay: 250,
	    data: function (params) {
	      return {
	        q: params.term, // search term
	        page: params.page
	      };
	    },
	    processResults: function (data, page) {
	      // parse the results into the format expected by Select2.
	      // since we are using custom formatting functions we do not need to
	      // alter the remote JSON data
	      return {
	        results: data.items
	      };
	    },
	    cache: true
	  },
	  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
	  minimumInputLength: 1
    });

});