<?php
require_once($CFG->libdir.'/gradelib.php');
class block_gradeoverride extends block_base {

    public function init() {
        $this->title = get_string('gradeoverride', 'block_gradeoverride');
    }

    protected function constructGradeItemSelector() {
    	global $CFG, $DB;
    	ob_start();
    	$gradeitems = $DB->get_records_sql("SELECT GI.*, C.fullname, C.category, C.idnumber as 'courseidnumber'
 											FROM {grade_items} GI, {course} C
 											WHERE GI.courseid = C.id
 											ORDER BY C.fullname ASC");
	 	$coursegradeitems = array();
	 	require_once($CFG->dirroot.'/lib/coursecatlib.php');
		$arrCategories = coursecat::make_categories_list();
	 	foreach ($gradeitems as $gradeitem) {

	 		$catname = $gradeitem->fullname;

	 		if (isset($arrCategories[$gradeitem->category])) {
	 			$catname = $arrCategories[$gradeitem->category] . ' /' . $gradeitem->fullname;
	 		}
	 		
	 		if (!isset($coursegradeitems[$catname])) {
	 			$coursegradeitems[$catname] = array();
	 		}
	 		$coursegradeitems[$catname][] = $gradeitem;
	 	}
	 	echo '<select id="block_gradeoverride_gradeitems" name="gradeitems[]" multiple required>';
	 	foreach ($coursegradeitems as $coursename => $gi) {
	 		echo '<optgroup label="' . $coursename . '">';
	 		foreach ($gi as $gradeitem) {
	 			$name = '';
	 			if (!empty($gradeitem->idnumber)) {
	 				$name .= '[' .$gradeitem->idnumber . '] ';
	 			}
	 			if ($gradeitem->itemtype == 'course') {
	 				if (!empty($gradeitem->courseidnumber)) {
	 					$name .= '[' . $gradeitem->courseidnumber . ']' . $gradeitem->fullname . ' (Course Final Grade)';
	 				} else {
	 					$name .= $gradeitem->fullname . ' (Course Final Grade)';
	 				}
	 				
	 			} else {
	 				if (!empty($gradeitem->itemname)) {
	 					$name .= $gradeitem->itemname . ' ';
	 				}
	 				if ($gradeitem->itemtype == 'manual') {
	 					$name .= '(Barcode Scan)';
	 				} else if ($gradeitem->itemmodule == 'scorm') {
	 					$name .= '(eLearning)';
	 				}
	 			}
	 			echo '<option value = ' . $gradeitem->id . '>' . $name . '</option>';
	 		}
	 		echo '</optgroup>';
	 	}
	 	echo '</select>';
    	return ob_get_clean();
    }

    public function constructGradeSelector() {
    	ob_start();
    	echo '<select id="block_gradeoverride_grade" name="grade" required>';
    	for ($i = 1; $i <= 100; $i++) {
    		$selected = '';
    		if ($i == 100) {
    			$selected = 'selected';
    		}
    		echo '<option value="' . $i . '" ' . $selected . '>' . $i . '</option>';
    	}
    	echo '</select>';
    	return ob_get_clean();
    }

    // The PHP tag and the curly bracket for the class definition 
    // will only be closed after there is another function added in the next section.

    public function get_content() {

    	global $CFG, $DB, $PAGE, $USER, $COURSE;

    	$PAGE->requires->jquery();

    	$PAGE->requires->js('/blocks/gradeoverride/gradeoverride.js');

		$PAGE->requires->css(new moodle_url('/blocks/gradeoverride/gradeoverride.css'));

		$PAGE->requires->css(new moodle_url('/blocks/gradeoverride/select2/dist/css/select2.css'));
		$PAGE->requires->js(new moodle_url('/blocks/gradeoverride/select2/dist/js/select2.js'));

    	$PAGE->requires->css('/blocks/gradeoverride/daterangepicker/daterangepicker-bs3.css');
		$PAGE->requires->js('/blocks/gradeoverride/daterangepicker/moment.min.js');
		$PAGE->requires->js('/blocks/gradeoverride/daterangepicker/daterangepicker.js');
		
	    if ($this->content !== null) {
	      return $this->content;
	    }
		$this->content =  new stdClass;
		
		$submit = optional_param('saveGrade',NULL, PARAM_INT);
		
		if(isset($submit)){
			$userid = optional_param_array('block_gradeoverride_userids', null, PARAM_INT);
			$grades = required_param('grade', PARAM_INT);
			$date = required_param('block_gradeoverride_dategraded', PARAM_TEXT);
			$date = DateTime::createFromFormat('d/m/Y', $date, new DateTimeZone('Australia/Sydney'));
			$date->modify('midnight');
			$gradeitem = optional_param_array('gradeitems', null, PARAM_INT);
			$transaction = $DB->start_delegated_transaction();			
			foreach($gradeitem as $gradeitems){
				$getItem = $DB->get_records_sql("SELECT GI.*, C.fullname, C.id, C.category, C.idnumber as 'courseidnumber'
 											FROM {grade_items} GI, {course} C
 											WHERE GI.courseid = C.id AND GI.id = ?
 											ORDER BY C.fullname ASC", array($gradeitems));
				foreach($userid as $empid){
					foreach($getItem as $gi){					
						
							$gg = $DB->get_record('grade_grades',array('userid'=>$empid, 'itemid'=>$gradeitems), '*', IGNORE_MULTIPLE);
						if($gg){
							$gg->finalgrade = $grades;
							$gg->feedback = 'Grade overriden!';
							$gg->feedbackformat = 1;
							$gg->overridden = 1;
							$gg->timemodified = $date->getTimestamp();
							$DB->update_record('grade_grades', $gg);
						}else{
							$record = new stdClass();
							$record->itemid   = $gradeitems;
							$record->rawgrade = $gi->grademin;
							$record->rawgrademax = $gi->grademax;
							$record->usermodified = $USER->id;
							$record->finalgrade = $grades;
							$record->overridden = 1;
							$record->timecreated = $date->getTimestamp();
							$record->itemid = $gradeitems;
							$record->userid = $empid;
							$record->feedback = 'Grade overriden!';
							$record->feedbackformat = 1;
							$DB->insert_record('grade_grades', $record, false);
						}
						
					}
				}
			
			}
			$transaction->allow_commit();
		}
	    
	    ob_start();
	    ?>

	    <form id="gradeoverrideform" action="#" method="POST">
	    	<label for="block_gradeoverride_userids">Employee ID(s)</label>
	    	<select id="block_gradeoverride_userids" name="block_gradeoverride_userids[]" multiple="true" required>
			</select>
	    	<label for="block_gradeoverride_gradeitems">Grade Item(s)</label>
	    	<?php echo self::constructGradeItemSelector(); ?>
	    	<label for="block_gradeoverride_grade">Grade</label>
	    	<?php echo self::constructGradeSelector(); ?>
	    	<label for="block_gradeoverride_dategraded">Date Graded</label>
	    	<input type="text" class="input-medium" id="block_gradeoverride_dategraded" name="block_gradeoverride_dategraded" value="<?php echo date('d/m/Y'); ?>" />
			<input type="submit" value="Save" name="saveGrade" class="btn btn-success">
		</form>

	    <?php
		
		
	    $content = ob_get_clean();
		
	    
	    $this->content->text .= $content;
	 
	    return $this->content;
	  }
	}   // Here's the closing bracket for the class definition